# TP2 : Manipulation de services

## Prérequis
### Nommer la machine
```bash=
sudo nano node1.tp2.linux
```
Depuis un nouveau terminal on remarque que le nom a changé : `hugo@node1:~$`
Ensuite on se déplace dans le fichier /etc puis : 
```bash=
sudo nano hostname
```
On change le nom par node1.tp2.linux
Pour vérification :
```bash=
cat hostname
```
Avec pour résultat :
`node1.tp2.linux`

### Configuration réseau
- Depuis la VM :
```bash=
ping 1.1.1.1
```
Donne comme résultat :  \
`[...]` \
`6 packets transmitted, 6 received, 0% packet loss, time 5010ms` \
`[...]`

```bash=
ping ynov.com
```
Donne comme résultat :  \
`[...]` \
`10 packets transmitted, 10 received, 0% packet loss, time 9014ms` \
`[...]`

- Depuis le PC :

```bash=
ping 192.168.56.129
```
Donne comme résultat :  \
`[...]` \
`Ping statistics for 192.168.56.129` \
`Packets : Sent = 4, Received = 4, Lost = 0 (0% loss),` \
`[...]`

## Partie 1 : SSH
### Installation du serveur

Pour installer ssh : 
```bash=
sudo apt install openssh-server
```
### Lancement du service SSH
```bash=
systemctl start ssh
```
Pour vérifier son fonctionnement :
```bash=
systemctl status ssh
```
Avec comme résultat :  \
`[...]` \
`Active: active (running) ...` \
`[...]`

### Etude du service SSH
- Pour afficher le status du service :
```bash=
systemctl status ssh
```
Avec comme résultat :  \
`[...]` \
`Active: active (running) ...` \
`[...]` \
`node1.tp2.linux sshd[1633]: server listening on 0.0.0.0 port 22.` \
`[...]`

- Pour afficher le processus liés au service ssh :
```bash=
ps -e | grep ssh
```
Donne pour résultat : \
`[...]` \
`1633 ?  00:00:00 sshd`

- Pour afficher le port utilisé par le service ssh :
```bash=
sudo ss -alnpt
```
Donne pour résultat : \
`[...]` \
`... 0.0.0.0:22 ... users: (("sshd",pid=1633, fd=3))` \
`[...]`
Donc le port utilisé est le **22**

- Pour afficher les logs du service ssh :
```bash=
journalctl -u ssh
```
Donne pour résultat : \
`[...]` \
`nov. 06 15:10:02 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.`

- Pour se connecter au serveur :
*Depuis le PC :*
```bash=
ssh hugo@192.168.56.129
```
Après avoir accepté et donné le mot de passe :
le terminal du pc commence par *hugo@node1* ici donc la connexion est faite.

### Modification de la configuration du serveur
- Modifier le comportement du service
```bash=
sudo nano etc/ssh/sshd_config
```
On effectue les changements demandés puis :
```bash=
cat etc/ssh/sshd_config
```
Donne : \
`[...]` \
`Port 3333` \
`[...]`

On redémarre le service avec 
```bash=
systemctl restart ssh
```

Puis pour vérifier que le port d'écoute est le bon :
```bash=
sudo ss -alnpt
```
Avec comme résultat : \
`[...]` \
`LISTEN ... 0.0.0.0:3333 ... users:(("sshd",pid=3671,fd=3))` \
`[...]`

- Pour se connecter au serveur ( depuis le PC ):
```bash=
ssh hugo@192.168.56.129 -p3333
```
La connexion est effectuée.


## Partie 2 : FTP
### Installation du serveur

Pour installer vsftpd : 
```bash=
sudo apt install vsftpd
```
### Lancement du service FTP
```bash=
systemctl start vsfptd
```
Pour vérifier son fonctionnement :
```bash=
systemctl status vsftpd.service
```
Avec comme résultat :  \
`[...]` \
`Active: active (running) ...` \
`[...]`

### Etude du service FTP
- Pour afficher le status du service :
```bash=
systemctl status vsftpd
```
Avec comme résultat :  \
`[...]` \
`Active: active (running) ...` \
`[...]` \
`node1.tp2.linux systemd[1]: Started vsftpd FTP server`


- Pour afficher le processus liés au service vsftpd :
```bash=
ps -e | grep vsftpd
```
Donne pour résultat : \
`[...]` \
`3942 ?  00:00:00 vsftpd`

- Pour afficher le port utilisé par le service vsftpd :
```bash=
sudo ss -alnpt
```
Donne pour résultat : \
`[...]` \
`... 0.0.0.0:21 ... users: (("vsftpd",pid=3942, fd=3))` \
`[...]`
Donc le port utilisé est le **21**

- Pour afficher les logs du service vsftpd :
```bash=
journalctl -u vsftpd
```
Donne pour résultat : \
`[...]` \
`nov. 06 16:18:08 node1.tp2.linux systemd[1]: Started vsftpd FTP server`

- Pour se connecter au serveur :

Depuis le pc en utilisant un client FTP ( ici FileZilla) et après avoir renseigné les informations ( Host = 192.168.56.129 User Password Port=21)
On peut download ( Pour l'exemple un fichier texte " test ") :
```bash=
sudo cat var/log/vsftpd.log
```
Avec comme résultat : \
`[...]` \
`... [pid 4694] [hugo] OK DOWNLOAD: "Client ::ffff:192.168.56.1", "/home/hugo/Desktop/test.txt", 17 bytes, 3.62Kbyte/sec` \
`[...]`

On peut upload ( Pour l'exemple un fichier texte " test2 ") :
( Pour cela il faut rendre écriture possible en enlevant le commentaire devant `#write_enable=YES` dans le fichier config )
```bash=
sudo cat var/log/vsftpd.log
```
Avec comme résultat : \
`[...]` \
`... [pid 5928] [hugo] OK UPLOAD: "Client ::ffff:192.168.56.1", "/home/hugo/Desktop/test2.txt", 0.00Kbyte/sec` \
`[...]`


### Modification de la configuration du serveur
- Modifier le comportement du service
```bash=
sudo nano etc/vsftpd.conf
```
On effectue les changements demandés puis :
```bash=
cat etc/vsftpd.conf
```
Donne : \
`listen_port=4444` \
`[...]` \
`write_enable=YES` \
`[...]`

On redémarre le service avec 
```bash=
systemctl restart vsftpd.service
```

Puis pour vérifier que le port d'écoute est le bon :
```bash=
sudo ss -alnpt
```
Avec comme résultat : \
`[...]` \
`LISTEN ... *:4444 ... users:(("vsftpd",pid=5964,fd=3))` \
`[...]`

- Pour se connecter au serveur :

Depuis le pc après avoir renseigné les informations ( Host = 192.168.56.129 User Password Port=4444)
On peut download :
```bash=
sudo cat var/log/vsftpd.log
```
Avec comme résultat : \
`[...]` \
`... [pid 5998] [hugo] OK DOWNLOAD: "Client ::ffff:192.168.56.1", "/home/hugo/Desktop/test.txt", 17 bytes, 5.22Kbyte/sec` \
`[...]`

On peut upload  :
```bash=
sudo cat var/log/vsftpd.log
```
Avec comme résultat : \
`[...]` \
`... [pid 5998] [hugo] OK UPLOAD: "Client ::ffff:192.168.56.1", "/home/hugo/Desktop/test2.txt", 0.00Kbyte/sec` \
`[...]`

## Partie 3 : Création d'un service
### Jouer avec Netcat

Etablir un chat :
- Sur la VM:
```bash=
nc -l 5555
```
- Sur le PC:
```bash=
nc 192.168.56.129 5555
```

Stocker les données échangées dans un fichier:


- Sur le PC avec le fichier ( ici filetest.txt)
```bash=
nc 192.168.56.129 5555 < filetest.txt
```

- Sur la VM ( qui va recevoir le fichier)
```bash=
nc -l 5555 > filetest.txt
```

On peut vérifier via :
```bash=
ls
```
Cela affiche : 
`... filetest.txt ...`

### Service basé sur Netcat
#### Créer un nouveau service

```bash=
sudo nano /etc/systemd/chat_tp2.service
```
Pour les permissions : 

```bash=
sudo chmod 777 chat_tp2.service
```

Contenu de chat_tp2.service :
```bash=
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 5555

[Install]
WantedBy=multi-user.target

```

#### Tester le nouveau service

- Démarrer le service :
```bash=
systemctl start chat_tp2.service
```

- Vérification du status :
```bash=
systemctl status chat_tp2.service
```
Donne pour résultat : \
`[...]` \
`Active: active (running) ...` \
`[...]` \
`... node1.tp2.linux systemd[1]: Started Little chat service (TP2)`

- Vérification du port d'écoute :
```bash=
sudo ss -alnpt
```
Avec pour résultat : \
`LISTEN 0 1 0.0.0.0:5555 0.0.0.0:* users:(("nc", pid=7416, fd=3))`

- Pour se connecter via le PC :
nc 192.168.56.129 5555

- Pour regarder les messages ( les logs) :
```bash=
systemctl status chat_tp2.service
```
ou
```bash=
journalctl -xe -u chat_tp2.service
```
ou
```bash=
journalctl -xe -u chat_tp2.service -f
```
Donne :  \
`[...]` \
`... node1.tp2.linux nc[7416]: message test`
