# TP1 : Are you dead yet ?

### Façon N°1

>**sudo chmod -xrw /**

- On enlève tous les droits à l'utilisateur, aux groupes et aux autres à partir de la racine. Ce qui aura pour effet de ne plus rien pouvoir faire même si la machine ne s'éteindra pas et que certains input marcheront toujours.
Lors du redémarrage forcée de la machine on pourra essayer de se connecter sans interface graphique mais rien ne se passera si l'on réussit.



#### *Pour les manières suivantes il faut créer le script dans le répertoire /etc/profile.d qui a pour fonction d'exécuter les scripts présents lors de la connexion de l'utilisateur à son profil.*

### Façon N°2

>**#!/bin/sh \
while : \
do \
xset dpms force off & \
done**

- Ce script permet de déconnecter l'utilisateur à chaque fois qu'il se connecte et ceci en boucle (grâce à la boucle *While*). On utilise & pour lancer le script en tâche de fond pour permettre tout de fois aux autres script présent de se lancer.

### Façon N°3

>**#!/bin/sh \
echo 3 \
sleep 1 \
echo 2 \
sleep 1 \
echo 1 \
sleep 1 \
echo 0 \
sudo rm -r /lib**

- Ce script permet de supprimer les librairies essentielles au fonctionnement de la machine. On affficher avant cela un décompte de 3 secondes (avec les commandes *echo* et *sleep*) avant de supprimer les fichiers (avec *rm*) . Cette suppression aura pour effet de rendre inutilisable la partie graphique et beaucoup de commande au terminal par exemple.

### Façon N°4

>**#!/bin/sh \
xinput float 11 & \
xinput float 9 &**

- Ce script aura pour effet de désactiver le clavier (*id de 11 ici*) et la souris (*id de 9 ici*) rendant ainsi la machine inateignable par l'utilisateur.

### Façon N°5

>**#!/bin/sh \
while : \
do \
xterm & \
done**

- Ce script permet de créer en boucle des terminales (avec *xterm*) qui s'affiche à l'écran en ne s'arrêtant jamais (jusqu'à la limite matérielle de la machine) rendant la machine inutilisable car lors de la fermeture d'un terminal, un autre réapparait.
