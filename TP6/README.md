# TP6 : Stockage et sauvegarde
## Partie 1 : Préparation de la machine backup.tp6.linux
### Ajout de disque
Ajouter un disque dur de 5Go à la VM
```bash=
[hugo@backup ~]$ lsblk | grep 5G
sdb           8:16   0    5G  0 disk
```
### Partitioning
Partitionner le disque à l'aide de LVM
```bash=
[hugo@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for hugo:
  Physical volume "/dev/sdb" successfully created.
[hugo@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g
```
```bash=
[hugo@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
[hugo@backup ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  backup   1   0   0 wz--n- <5.00g <5.00g
```
```bash=
[hugo@backup ~]$ sudo lvcreate -l 100%FREE backup
  Logical volume "lvol0" created.
[hugo@backup ~]$ sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lvol0 backup -wi-a-----  <5.00g
  [...]
```
Formater la partition
```bash=
[hugo@backup ~]$ sudo mkfs -t ext4 /dev/backup/lvol0
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: 531fc03e-ce07-4148-82e0-912ccb16db08
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```
Monter la partition
```bash=
[hugo@backup ~]$ sudo mount /dev/backup/lvol0 /backup
[hugo@backup ~]$ df -h
Filesystem                Size  Used Avail Use% Mounted on
devtmpfs                  387M     0  387M   0% /dev
[...]
/dev/mapper/backup-lvol0  4.9G   20M  4.6G   1% /backup
[hugo@backup ~]$ cd /backup/
[hugo@backup backup]$ sudo nano test
[hugo@backup backup]$ cat test
coucou
```
```bash=
[hugo@backup backup]$ sudo nano /etc/fstab
[hugo@backup backup]$ cat /etc/fstab
[...]
/dev/backup/lvol0 /backup ext4 defaults 0 0
[hugo@backup backup]$ cd
[hugo@backup ~]$ sudo umount /backup
[hugo@backup ~]$ sudo mount -av
[...]
/backup                  : successfully mounted
```

### Bonus
```bash=
[hugo@backup ~]$ sudo pvcreate /dev/sdc
[sudo] password for hugo:
  Physical volume "/dev/sdc" successfully created.
[hugo@backup ~]$ sudo pvdisplay
[...]
  "/dev/sdc" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               5.00 GiB
[...]

[hugo@backup ~]$ sudo vgextend backup /dev/sdc
  Volume group "backup" successfully extended
[hugo@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup
  [...]
  Cur PV                2
  Act PV                2
  VG Size               9.99 GiB
  [...]
  
[hugo@backup ~]$ sudo lvextend -l +100%FREE /dev/backup/lvol0
  Size of logical volume backup/lvol0 changed from <5.00 GiB (1279 extents) to 9.99 GiB (2558 extents).
  Logical volume backup/lvol0 successfully resized.
  
[hugo@backup ~]$ sudo resize2fs /dev/backup/lvol0
resize2fs 1.45.6 (20-Mar-2020)
Filesystem at /dev/backup/lvol0 is mounted on /backup; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 2
The filesystem on /dev/backup/lvol0 is now 2619392 (4k) blocks long.
[hugo@backup ~]$ df -h
Filesystem                Size  Used Avail Use% Mounted on
[...]
/dev/mapper/backup-lvol0  9.8G   23M  9.3G   1% /backup
[...]
```

## Partie 2 : Setup du serveur NFS sur backup.tp6.linux
Préparer les dossiers à partager
```bash=
[hugo@backup ~]$ sudo mkdir /backup/web.tp6.linux
[hugo@backup ~]$ sudo mkdir /backup/db.tp6.linux
```
 Install du serveur NFS
```bash=
[hugo@backup ~]$ sudo dnf install nfs-utils
[...]
Complete!
``` 
Conf du serveur NFS
```bash=
[hugo@backup ~]$ sudo nano /etc/idmapd.conf
[sudo] password for hugo:
[hugo@backup ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
[...]
[hugo@backup ~]$ sudo nano /etc/exports
[hugo@backup ~]$ cat /etc/exports
/backup/web.tp6.linux/ 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux/ 10.5.1.0/24(rw,no_root_squash)
```
On spécifie rw pour avoir la possibilité d'écrire en plus au lieu de n'être qu'en lecture seule (ro) pour pouvoir autoriser les hôtes distants ( les deux autres VM ici ) à modifier ces fichiers
Le no_root_squash permet d'éviter de réduire les permissions des roots possiblement connectées à distance à de simple utilisateur et donc d'avoir la possibilité d'avoir des privilèges roots avec les deux autres VM sur les deux fichiers 

Démarrez le service
```bash=
[hugo@backup ~]$ sudo systemctl start nfs-server.service
[sudo] password for hugo:
[hugo@backup ~]$ systemctl status nfs-server.service
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-11-30 13:25:41 CET; 3s ago
  [...]
[hugo@backup ~]$ sudo systemctl enable nfs-server.service
```

Firewall
 
```bash=
[hugo@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
[hugo@backup ~]$ sudo firewall-cmd --reload
success
```
 
```bash=
[hugo@backup ~]$ sudo ss -alnpt | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```

## Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux

Install
```bash=
[hugo@web ~]$ sudo dnf install nfs-utils
Complete!
```

Conf
```bash=
[hugo@web ~]$ sudo mkdir /srv/backup
[hugo@web ~]$ sudo nano /etc/idmapd.conf
[hugo@web ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
[...]
```

Montage
```bash=
[hugo@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup
[hugo@web ~]$ df -h
Filesystem                       Size  Used Avail Use% Mounted on
[...]
10.5.1.13:/backup/web.tp6.linux  9.8G   23M  9.3G   1% /srv/backup
[hugo@web /]$ sudo nano /srv/backup/test
[hugo@web /]$ cat /srv/backup/test
test
[hugo@web /]$ sudo nano /etc/fstab
[hugo@web /]$ cat /etc/fstab
[...]
10.5.1.13:/backup/web.tp6.linux/ /srv/backup nfs4 defaults 0 0
[hugo@web /]$ sudo mount -av
[...]
/srv/backup                   : successfully mounted
```

Répétez les opérations sur db.tp6.linux
```bash=
[hugo@db ~]$ df -h
Filesystem                      Size  Used Avail Use% Mounted on
[...]
10.5.1.13:/backup/db.tp6.linux  9.8G   23M  9.3G   1% /srv/backup
[hugo@db ~]$ sudo nano /srv/backup/test
[hugo@db ~]$ cat /srv/backup/test
test2
[hugo@db ~]$ sudo mount -av
[...]
/srv/backup              : successfully mounted
```

## Partie 4 : Scripts de sauvegarde
### Sauvegarde Web

Script qui sauvegarde les données de NextCloud
```bash=
[hugo@web ~]$ sudo nano /srv/backupweb.sh
[hugo@web ~]$ cat /srv/backupweb.sh
#!/bin/bash
#Make backup of nextcloud
# hugo

# Script vars
save_file='/srv/backup/'
log_file='/var/log/backup/backup.log'
at_save='/var/www/nextcloud'


usage() {
echo "Usage : backupweb.sh
Make a backup of nextcloud file at ${save_file} in tar.gz

        -h      Prints this message"
}

while getopts ":h" option; do
    case "${option}" in
        h)
            usage
            exit 0
            ;;
    esac
done

name=nextcloud_$(date +"%y%m%d_%H%M%S").tar.gz
tar -czvf ${name} ${at_save} &> /dev/null
mv ${name} ${save_file}

#log

log_prefix=$(date +"[%y/%m/%d %H:%M:%S]")
log_line="${log_prefix} Backup ${save_file}${name} created successfully."
echo "${log_line}" >> "${log_file}"
echo "Backup ${save_file}${name} created successfully."
[hugo@web ~]$ sudo bash /srv/backupweb.sh
Backup /srv/backup/nextcloud_211203_121036.tar.gz created successfully.
[hugo@web ~]$ cat /var/log/backup/backup.log
[21/12/03 12:03:55] Backup /srv/backup/nextcloud_211203_120256.tar.gz created successfully
[21/12/03 12:11:24] Backup /srv/backup/nextcloud_211203_121036.tar.gz created successfully.
[hugo@web ~]$ ls /srv/backup
nextcloud_211203_121036.tar.gz
```

Créer un service
```bash=
[hugo@web ~]$ sudo nano /etc/systemd/system/backup.service
[hugo@web ~]$ sudo cat /etc/systemd/system/backup.service
[Unit]
Description=Backup for Nextcloud

[Service]
ExecStart=/srv/backupweb.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
[hugo@web ~]$ sudo systemctl start backup.service
[hugo@web ~]$ systemctl status backup.service
● backup.service - Backup for Nextcloud
   Loaded: loaded (/etc/systemd/system/backup.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[...]
Dec 07 14:53:00 web.tp6.linux systemd[1]: Starting Backup for Nextcloud...
Dec 07 14:54:09 web.tp6.linux backupweb.sh[1907]: Backup /srv/backup/nextcloud_211207_145300.tar.gz created successfully.
Dec 07 14:54:09 web.tp6.linux systemd[1]: backup.service: Succeeded.
Dec 07 14:54:09 web.tp6.linux systemd[1]: Started Backup for Nextcloud.
```

Vérifier la restauration des données
```bash=
[hugo@web ~]$ sudo mv /srv/backup/nextcloud_211207_145300.tar.gz /
[hugo@web ~]$ sudo rm -r /var/www/nextcloud
[hugo@web /]$ sudo tar -xvf /var/www/nextcloud_211207_145300.tar.gz
[hugo@web /]$ ls var/www/nextcloud/html/
3rdparty  AUTHORS  console.php  core      data        index.php  nextcloud  ocm-provider  ocs-provider  remote.php  robots.txt  themes   version.php
apps      config   COPYING      cron.php  index.html  lib        occ        ocs           public.php    resources   status.php  updater
```

Créer un timer
```bash=
[hugo@web ~]$ sudo nano /etc/systemd/system/backup.timer
[hugo@web ~]$ sudo systemctl daemon-reload
[hugo@web ~]$ sudo systemctl start backup.timer
[hugo@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[hugo@web ~]$ sudo systemctl list-timers
NEXT                         LEFT          LAST                         PASSED    UNIT                         ACTIVATES
Tue 2021-12-07 16:00:00 CET  43min left    n/a                          n/a       backup.timer                 backup.service
[...]
```

### Sauvegarde base de données
Script qui sauvegarde les données de la base de données MariaDB

```bash=
[hugo@db ~]$ sudo nano /srv/backupdb.sh
[hugo@db ~]$ sudo cat /srv/backupdb.sh
#!/bin/bash
#Make backup of database
# hugo

# Script vars
save_file='/srv/backup/'
log_file='/var/log/backup/backup_db.log'


usage() {
echo "Usage : backupdb.sh
Make a backup of database more specifically of nextcloud in tar.gz

        -h      Prints this message"
}

while getopts ":h" option; do
    case "${option}" in
        h)
            usage
            exit 0
            ;;
    esac
done

name=nextcloud_db_$(date +"%y%m%d_%H%M%S").tar.gz
mysqldump -h 10.5.1.12 -p -u nextcloud nextcloud > ${save_file}tempdb.sql
tar -czvf ${name} ${save_file}tempdb.sql &> /dev/null
rm ${save_file}tempdb.sql
mv ${name} ${save_file}

#log

log_prefix=$(date +"[%y/%m/%d %H:%M:%S]")
log_line="${log_prefix} Backup ${save_file}${name} created successfully."
echo "${log_line}" >> "${log_file}"
echo "Backup ${save_file}${name} created successfully."
[hugo@db ~]$ sudo bash /srv/backupdb.sh
Enter password:
Backup /srv/backup/nextcloud_db_211207_155232.tar.gz created successfully.
[hugo@db ~]$ cat /var/log/backup/backup_db.log
[21/12/07 15:46:10] Backup /srv/backup/nextcloud_db_211207_154607.tar.gz created successfully.
[21/12/07 15:49:39] Backup /srv/backup/nextcloud_db_211207_154936.tar.gz created successfully.
[21/12/07 15:52:35] Backup /srv/backup/nextcloud_db_211207_155232.tar.gz created successfully.
[hugo@db ~]$ ls /srv/backup
nextcloud_db_211207_154321.tar.gz  nextcloud_db_211207_154607.tar.gz  nextcloud_db_211207_154936.tar.gz  nextcloud_db_211207_155232.tar.gz
```

Créer un service
```bash=
[hugo@db ~]$ sudo nano /etc/systemd/system/backup_db.service
[hugo@db ~]$ sudo cat /etc/systemd/system/backup_db.service
[Unit]
Description=Backup database for Nextcloud

[Service]
ExecStart=/srv/backupdb.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
[hugo@db ~]$ sudo systemctl start backup_db.service
[hugo@db ~]$ ls /srv/backup
[...] nextcloud_db_211207_160333.tar.gz
```

Créer un timer
```bash=
[hugo@db ~]$ sudo nano /etc/systemd/system/backup_db.timer
[hugo@db ~]$ sudo systemctl daemon-reload
[hugo@db ~]$ sudo systemctl start backup_db.timer
[hugo@db ~]$ sudo systemctl enable backup_db.timer
Created symlink /etc/systemd/system/timers.target.wants/backup_db.timer → /etc/systemd/system/backup_db.timer.
[hugo@db ~]$ sudo systemctl list-timers
NEXT                         LEFT       LAST                         PASSED       UNIT                         ACTIVATES
Tue 2021-12-07 17:00:00 CET  50min left n/a                          n/a          backup_db.timer              backup_db.service
[...]
```
