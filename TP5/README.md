# TP5: P'tit cloud perso
## Setup DB
### Install MariaDB
```bash=
[hugo@db ~]$ sudo dnf install mariadb-server
[...]
Complete!
```

Service MariaDB
```bash=
[hugo@db ~]$ sudo systemctl start mariadb.service
```
```bash=
[hugo@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```bash=
[hugo@db ~]$ systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 11:39:01 CET; 1min 27s ago
   [...]
```

```bash=
[hugo@db ~]$ sudo ss -alnpt | grep sql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=4583,fd=21))
```
Donc le numéro de port est *3306* avec comme processus derrière *mysqld*

```bash=
[hugo@db ~]$ ps -aux | grep sql
mysql       4583  0.0 11.1 1296832 92216 ?       Ssl  11:38   0:00 /usr/libexec/mysqld --basedir=/usr
```
Donc le process est lancé sous l'utilisateur *mysql*

Conf firewall

```bash=
[hugo@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[hugo@db ~]$ sudo firewall-cmd --reload
success
```

### Conf MariaDB

Configuration élementaire de base
```bash=
mysql_secure_installation
[...]
Enter current password for root (enter for none):
OK, successfully used password, moving on...
#Pas encore de password pour le root donc on passe
[...]

Set root password? [Y/n] y
#On veut donc definir un password pour l'user root pour garantir une connexion avec autorisation à cette user
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

[...]
#On veut supprimer la possibilité de se connecter anonymement à la DB et donc avoir la nécessité d'avoir un compte utilisateur pour , par exemple eviter q'un bot se connecte avec beaucoup trop d'anonyme et surcharge la DB
Remove anonymous users? [Y/n] y
 ... Success!

[...]
#On veut désautoriser la possibilité de se connecter à root en dehors de localhost pour, par exemple éviter que le password de root soit trouvé plus facilement lorsque l'on se connectera à un réseau pas sécur
Disallow root login remotely? [Y/n] y
 ... Success!

[...]
#On veut supprimer une database accesible par tout le monde
Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

[...]
#On veut reload la table des privilèges pour que les changements effectués prennent effets directement
Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

[...]

Thanks for using MariaDB!
```

Préparation de la base en vue de l'utilisation par NextCloud
```bash=
[hugo@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.001 sec)
FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
### Test
installer mysql sur web.tp5.linux
```bash=
[hugo@web ~]$ dnf provides mysql
[...]
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[hugo@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
[...]
Complete!
```

Tester la connexion

```bash=
[hugo@web ~]$ mysql --host=10.5.1.12 --port=3306 --user=nextcloud --database=nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
[...]
mysql> SHOW TABLES
    ->
```

## Setup Web

### Install Apache
Apache
```bash=
[hugo@web ~]$ sudo dnf install httpd
[...]
Complete!
[hugo@web ~]$ sudo systemctl start httpd.service
[hugo@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-26 09:28:02 CET; 9s ago
```
Analyse du service
Pour activer le service au démarrage
```bash=
[hugo@web ~]$ systemctl enable httpd
```
Processus lié au service
```bash=
[hugo@web ~]$ ps -aux | grep httpd
root        2585  0.0  1.3 280220 11252 ?        Ss   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2586  0.0  1.0 294104  8452 ?        S    09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2587  0.1  1.7 1483020 14228 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2588  0.1  1.4 1351892 12180 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2589  0.1  1.4 1351892 12180 ?       Sl   09:28   0:00 /usr/sbin/httpd -DFOREGROUND
```
Ainsi les processus sont lancés par l'utilisateur *apache*

Port d'écoute d'Apache par défaut
```bash=
[hugo@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2589,fd=4),("httpd",pid=2588,fd=4),("http",pid=2587,fd=4),("httpd",pid=2585,fd=4))
```
Donc le port d'écoute par défaut est le *80*

Premier test
Ouverture du port dans le firewall
```bash=
[hugo@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --reload
success
```
Test
```bash=
curl http://10.5.1.11:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    [...]
```

PHP

Installer php
```bash=
[hugo@web ~]$ sudo dnf install epel-release
[...]
Complete!
[hugo@web ~]$ sudo dnf update
[...]
Complete!
[hugo@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
Complete!
sudo dnf module enable php:remi-7.4
[...]
Complete!
[hugo@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
Complete!
```

### Conf Apache

Analyser conf Apache
```bash=
[hugo@web ~]$ cat /etc/httpd/conf/httpd.conf | grep conf
[...]
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```
Créer un VirtualHost
```bash=
[hugo@web conf.d]$ sudo nano nextcloud.conf
[hugo@web conf.d]$ cat nextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/
```

Configurer racine web
```bash=
[hugo@web www]$ sudo mkdir -p nextcloud/html
[hugo@web www]$ sudo chown apache nextcloud/
[hugo@web www]$ sudo chown apache nextcloud/html/
```

Configurer php
```bash=
[hugo@web www]$ timedatectl
               Local time: Fri 2021-11-26 10:27:14 CET
           Universal time: Fri 2021-11-26 09:27:14 UTC
                 RTC time: Fri 2021-11-26 09:27:12
                Time zone: Europe/Paris (CET, +0100)
                [...]
[hugo@web ~]$ sudo nano /etc/opt/remi/php74/php.ini
[hugo@web ~]$ cat /etc/opt/remi/php74/php.ini | grep date
[...]
date.timezone = "Europe/Paris"
[...]
```

### Install Nextcloud

Récuperer nextcloud
```bash=
[hugo@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  15.7M      0  0:00:09  0:00:09 --:--:-- 18.9M
[hugo@web ~]$ ls
nextcloud-21.0.1.zip
```

Ranger sa chambre
```bash=
[hugo@web ~]$ unzip nextcloud-21.0.1.zip
[hugo@web ~]$ sudo mv nextcloud/* /var/www/nextcloud/html/
[hugo@web html]$ sudo chown -R apache html/
[hugo@web ~]$ rm nextcloud-21.0.1.zip
```

### Test
Modifier le fichier host du PC
```bash=
C:\Windows\System32\drivers\etc>type hosts
#
10.5.1.11 web.tp5.linux
[...]
```

Tester l'accès à son nextcloud

On se connecte à *http://web.tp5.linux*
Si c'est la première fois on dois créer un user admin et configurer la base de données comme indiqué avec les informations suivantes :
user : nextcloud avec son mot de passe
database name : nextcloud
ip avec le port de la database : 10.5.1.12:3306
