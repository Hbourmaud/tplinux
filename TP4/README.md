# TP4 : Une distribution orientée serveur

## Checklist

**Contenu du fichier conf pour l'ip :**
```bash=
[hugo@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.200.1.37
NETMASK=255.255.255.0
```

```bash=
[hugo@localhost ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
link/ether 08:00:27:b1:12:e8 brd ff:ff:ff:ff:ff:ff
inet 10.200.1.37/24 brd 10.200.1.255 scope global noprefixroute enp0s8
[...]
```

**Connexion SSH**
```bash=
[hugo@localhost ~]$ systemctl status sshd
sshd.service - OpenSSH server daemon
Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
Active: active (running) since Tue 2021-11-23 14:20:13 CET; 11min ago
[...]
```
```bash=
[hugo@localhost ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP6UnIxxw74ed7plUBZ7myGHVrhdoWsd0MhBhSLBYUzB69Ss+iWIzhVKhtLwk/oNXj+Ur12KglKpVzpdXFBPl7quonBsInMSFiZiBjm2AqLbJWUSSYOVh12gHeBuXTaIpseobIJkKUq4eLohxf8vw8MhhfIHofqEZdztLsjih8kBG6DON/dlLnz02ItHmmmmm8BdAmUF1lxgSvzGwOvtpVR8/8AX8HgxeMqlmTLmCaGOOjs4J+FZlh1i1Sah+iCgfNzmU0YgiYLK/P38BUyGrOlmAsb4P5fOW3vwRNKaqfgbshT64fLO+4xUWLrAEM79yQam4lu9xmCsiV5QCpgaviR0DHt7kncz2csmt4XyooPrTceXSHXpSXJjqrmC9wLbHbygqiSB9DkbvBxwxTzc2QZrYxb8p/RgVQrbFrlUzpM9ugX2jzKvOq6vWpjd5VDUdKmTH6NN8mbwtW32dOMMSquTJ1GMXyo+M8i+rC+yg32C0G4gEnMdouAgf+F73Bn7YX7zDH/EqBKezXtmwBBTKc7YbwyVGiDPPNTEFG7mw0ACcVCzB60xU9nPUEOLXTY+AKXSQPb/hgbeGMgtcgkfpIb0KbqxIIf3eMwdd2fg+IA65koh4PEMEDyh+i8XUQTADreQ3IUwTHU+/OT7XZpbjK+UrmG6grXwQax+FvfDZ8jQ== user@DESKTOP-RUFUR0T
```
```bash=
cat .ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP6UnIxxw74ed7plUBZ7myGHVrhdoWsd0MhBhSLBYUzB69Ss+iWIzhVKhtLwk/oNXj+Ur12KglKpVzpdXFBPl7quonBsInMSFiZiBjm2AqLbJWUSSYOVh12gHeBuXTaIpseobIJkKUq4eLohxf8vw8MhhfIHofqEZdztLsjih8kBG6DON/dlLnz02ItHmmmmm8BdAmUF1lxgSvzGwOvtpVR8/8AX8HgxeMqlmTLmCaGOOjs4J+FZlh1i1Sah+iCgfNzmU0YgiYLK/P38BUyGrOlmAsb4P5fOW3vwRNKaqfgbshT64fLO+4xUWLrAEM79yQam4lu9xmCsiV5QCpgaviR0DHt7kncz2csmt4XyooPrTceXSHXpSXJjqrmC9wLbHbygqiSB9DkbvBxwxTzc2QZrYxb8p/RgVQrbFrlUzpM9ugX2jzKvOq6vWpjd5VDUdKmTH6NN8mbwtW32dOMMSquTJ1GMXyo+M8i+rC+yg32C0G4gEnMdouAgf+F73Bn7YX7zDH/EqBKezXtmwBBTKc7YbwyVGiDPPNTEFG7mw0ACcVCzB60xU9nPUEOLXTY+AKXSQPb/hgbeGMgtcgkfpIb0KbqxIIf3eMwdd2fg+IA65koh4PEMEDyh+i8XUQTADreQ3IUwTHU+/OT7XZpbjK+UrmG6grXwQax+FvfDZ8jQ== user@DESKTOP-RUFUR0T
```
Connexion sans mot de passe
```bash=
ssh hugo@10.200.1.37
[hugo@localhost ~]$
```

**Accès internet :**
```bash=
[hugo@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=59 time=11.3 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=59 time=11.6 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=59 time=11.6 ms
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
```
```bash=
[hugo@localhost ~]$ ping www.ynov.com
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=56 time=11.4 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=56 time=33.3 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=56 time=10.9 ms

3 packets transmitted, 3 received, 0% packet loss, time 2004ms
```

**Nommage de la machine**

```bash=
[hugo@localhost ~]$ cat /etc/hostname
node1.tp4.linux
```
```bash=
[hugo@localhost ~]$ hostname
node1.tp4.linux
```

## Mettre en place un service : Nginx

**Pour l'installation :**
On regarde si il n'est pas deja installé :
```bash=
[hugo@node1 ~]$ rpm -q nginx
package nginx is not installed
```
On l'installe :
```bash=
[hugo@node1 ~]$ sudo dnf install nginx -y
```

**Analysez le service:**

```bash=
[hugo@node1 ~]$ ps -aux | grep nginx
[...]
nginx       8140  0.0  0.9 151820  8256 ?        S    14:53   0:00 nginx: worker process
```
Donc le processus du service tourne sous l'utilisateur *nginx*

```bash=
[hugo@node1 ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=8140,fd=8),("nginx",pid=8139,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=8140,fd=9),("nginx",pid=8139,fd=9))
```
Donc le port qui écoute est le *80*

```bash=
[hugo@node1 ~]$ cat /etc/nginx/nginx.conf | grep root
root         /usr/share/nginx/html;
root         /usr/share/nginx/html;
```

Donc la racine web se trouve dans */usr/share/nginx/html*

```bash=
[hugo@node1 ~]$ ls -la /usr/share/nginx/html/
drwxr-xr-x. 2 root root   99 Nov 23 14:52 .
drwxr-xr-x. 4 root root   33 Nov 23 14:52 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```
On remarque que tous le monde a les droits de lectures donc également pour l'utilisateur lançant le processus

## Visite du service WEB

**Configuration du firewall**

On autorise les connexions sur le port TCP 80
```bash=
[hugo@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

et on reload pour effectuer les modifications 
```bash=
[hugo@node1 ~]$ sudo firewall-cmd --reload
success
```

**Fonctionnement du service**

```bash=
[hugo@node1 ~]$ curl http://10.200.1.37:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    [...]
```

## Modification configuration serveur

**Changer port d'écoute**
```bash=
[hugo@node1 ~]$ cat /etc/nginx/nginx.conf
[...]
server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
[...]
```

```bash=
[hugo@node1 ~]$ sudo systemctl restart nginx
systemctl status nginx
nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-23 15:13:20 CET; 9s ago
   [...]
```

Nouveau port affecté
```bash=
[hugo@node1 ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=8276,fd=8),("nginx",pid=8275,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=8276,fd=9),("nginx",pid=8275,fd=9))
```

Fermeture de l'ancien port

```bash=
[hugo@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```

Ouverture du port 8080

```bash=
[hugo@node1 ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
```

```bash=
[hugo@node1 ~]$ curl http://10.200.1.37:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
  [...]
```

**Changer l'utilisateur qui lance le service**

Création de l'utilisateur web avec comme homedir */home/web* et mot de passe *123*

```bash=
[hugo@node1 ~]$ sudo useradd web -m -d /home/web -s /bin/sh -u 200 -p 123
```

Changer l'utilisateur qui lance le service

```bash=
[hugo@node1 ~]$ cat /etc/nginx/nginx.conf
[...]
user web;
[...]
```
Redémarrage du service
```bash=
[hugo@node1 ~]$ sudo systemctl restart nginx.service
```
```bash=
[hugo@node1 ~]$ sudo ps -aux | grep nginx
[...]
web         8456  0.0  0.9 151820  8156 ?        S    15:26   0:00 nginx: worker process
```

**Changer l'emplacement de la racine web**

Si nécessaire, on créée le dossier /var/www/
```bash=
[hugo@node1 ~]$ sudo mkdir /var/www/
```

On change d'utilisateur pour créer les autres dossiers/fichiers devant lui appartenir

```bash=
[hugo@node1 ~]$ su - web
```
```bash=
[web@node1 ~]$ mkdir /var/www/super_site_web
```
```bash=
[web@node1 ~]$ nano /var/www/super_site_web/index.html
```
On écrit le code html voulu

Pour vérifier l'appartenance des fichiers
```bash=
[web@node1 ~]$ ls -la /var/www/super_site_web/
drwxrwxr-x. 2 web  web  24 Nov 23 15:36 .
drwxrwxrwx. 3 root root 28 Nov 23 15:35 ..
-rw-rw-r--. 1 web  web  16 Nov 23 15:36 index.html
```

Modifier la racine web pour la nouvelle
```bash=
[hugo@node1 ~]$ sudo nano /etc/nginx/nginx.conf
```
```bash=
[hugo@node1 ~]$ cat /etc/nginx/nginx.conf
[...]
server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web;
[...]
```

```bash=
[hugo@node1 ~]$ sudo systemctl restart nginx.service
```

Prouver le changement de racine
```bash=
[hugo@node1 ~]$ curl http://10.200.1.37:8080
<h1>coucou</h1>
```
